# Windows SMB File Share Connector

The Windows SMB Connector provides native access to a Windows file share via the Server Message Block (SMB) protocol. The most common usecase is for accessing a Windows file share from a Linux Molecule without requiring Samba to be installed.

# Connection Tab

## Connection Fields


### Host Name

Server name for the file share

**Type** - string




### Port

Host port

**Type** - integer

**Default Value** - 445




### User Name

User name for Windows Authentication

**Type** - string




### Password

Password for Windows Authentication.

**Type** - password




### Domain Name

Windows Domain

**Type** - string




### Transaction Timeout

Timeout sets Read, Write, and Transact timeouts (default is 60 seconds)

**Type** - integer

**Default Value** - 60




### Socket Timeout

Socket Timeout (default is 0 seconds, blocks forever)

**Type** - integer

**Default Value** - 0




### Share Name

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string


# Operation Tab



## Read
### Operation Fields



#### Directory

The folder for the file. Note, do not include neither a leading or trailing \ character.

**Type** - string




#### File Name Pattern

Specify the name of the file to read. Wild cards are supported to read multiple files. The name of the file will be set in the File Name Document Property

**Type** - string




#### Append to Name After Read

If specified, after reading, the specified extension will be appended to the file name

**Type** - string




## Write
### Operation Fields



#### Directory

The folder for the file. Note, do not include neither a leading or trailing \ character.

**Type** - string




#### Action if File Exists

Specify how the operation behaves if the file name already exists in the directory. Select Create unique name' to append an incremental number to the file name until finding a name that does not exist. Select 'Overwrite' to replace the existing file with a new one. Select 'Append' to append new data to the end of the file. Select 'Generate error' to generate an error and not upload the file.

**Type** - string

**Default Value** - CREATE_UNIQUE

##### Allowed Values
 * Create unique name
 * Overwrite
 * Generate error



## List Files
### Operation Fields



#### Directory

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string




#### Search Pattern

 **helpText NOT SET IN DESCRIPTOR FILE**

**Type** - string

**Default Value** - *.*



# Inbound Document Properties
Inbound document properties can set by a process before a connector shape to control options supported by the connector.

 * **File Name** -  **helpText NOT SET IN DESCRIPTOR FILE**


# Outbound Document Properties

Outbound document properties can used by a process after a connector shape to access information set by the connector.

 * **File Name** -  **helpText NOT SET IN DESCRIPTOR FILE**

