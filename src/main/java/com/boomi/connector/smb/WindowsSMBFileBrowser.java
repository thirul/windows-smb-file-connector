package com.boomi.connector.smb;

import java.util.Collection;

import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.util.BaseBrowser;

public class WindowsSMBFileBrowser extends BaseBrowser implements ConnectionTester{

    protected WindowsSMBFileBrowser(WindowsSMBFileConnection conn) {
        super(conn);
    }

	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
			Collection<ObjectDefinitionRole> roles) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectTypes getObjectTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void testConnection() {
		try {
			this.getConnection().testConnection();
        }
        catch (Exception e) {
            throw new ConnectorException("Could not establish a connection", e);
        }
	}

	@Override
    public WindowsSMBFileConnection getConnection() {
        return (WindowsSMBFileConnection) super.getConnection();
    }
}