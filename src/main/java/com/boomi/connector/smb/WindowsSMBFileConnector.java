package com.boomi.connector.smb;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;

public class WindowsSMBFileConnector extends BaseConnector {

    @Override
    public Browser createBrowser(BrowseContext context) {
        return new WindowsSMBFileBrowser(createConnection(context));
    }    

    @Override
    protected Operation createGetOperation(OperationContext context) {
        return null;
    }

    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return null;
    }

    @Override
    protected Operation createCreateOperation(OperationContext context) {
        return null;
    }

    @Override
    protected Operation createUpdateOperation(OperationContext context) {
        return null;
    }

    @Override
    protected Operation createDeleteOperation(OperationContext context) {
        return null;
    }

    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new WindowsSMBFileExecuteOperation(createConnection(context));
    }
   
    private WindowsSMBFileConnection createConnection(BrowseContext context) {
        return new WindowsSMBFileConnection(context);
    }
}