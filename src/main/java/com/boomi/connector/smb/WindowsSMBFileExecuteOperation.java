package com.boomi.connector.smb;

import java.io.ByteArrayInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystemException;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.StringUtil;
import com.hierynomus.msdtyp.AccessMask;
import com.hierynomus.msfscc.fileinformation.FileIdBothDirectoryInformation;
import com.hierynomus.mssmb2.SMB2CreateDisposition;
import com.hierynomus.mssmb2.SMB2CreateOptions;
import com.hierynomus.mssmb2.SMB2ShareAccess;
import com.hierynomus.protocol.commons.IOUtils;
import com.hierynomus.smbj.share.DiskShare;
import com.hierynomus.smbj.share.File;

public class WindowsSMBFileExecuteOperation extends BaseUpdateOperation {
    public enum OperationProperties {DIRECTORY,FILENAME,SEARCHPATTERN,IF_FILE_EXISTS,THROWERROR,APPEND,OVERWRITE,CREATE_UNIQUE,RENAMEAFTERREAD};
//    public enum DocumentProperties {DIRECTORY,FILENAME,SEARCHPATTERN,IFFILEXISTS,THROWERROR,APPEND,OVERWRITE,CREATE_UNIQUE};
    protected WindowsSMBFileExecuteOperation(WindowsSMBFileConnection conn) {
		super(conn);
	}
    
    //TODO dump this to a JSON profile, not a text blob
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
	    Logger logger = response.getLogger();
	    String fileName;
	    String path;
	    getConnection().setLogger(logger);
		DiskShare share = getConnection().connect();
		if (share==null)
			throw new ConnectorException("Error connecting to server");
	    
		PropertyMap opProps = getContext().getOperationProperties();
		
	    String directoryName = opProps.getProperty(OperationProperties.DIRECTORY.name(), "");
	    InputStream is = null;
//	    if (StringUtil.isEmpty(directoryName))
//	    	throw new ConnectorException("Directory name is required");
	    for (ObjectData input : request)
	    {
		    switch (getContext().getCustomOperationType())
		    {
//			    case "DELETE":
//				    fileName = opProps.getProperty(OperationProperties.FILENAME.name());
//				    if (StringUtil.isEmpty(fileName))
//				    	fileName = input.getDynamicProperties().get(OperationProperties.FILENAME.name());
//				    if (StringUtil.isEmpty(fileName))
//				    	throw new ConnectorException("File name is required");
//				    path = directoryName+fileName;
//				    logger.info(path);
//			        share.rm(path);
//			        response.addEmptyResult(null, OperationStatus.SUCCESS, "OK",
//		            		"OK");
//			    	break;
			    case "LIST":
				    String searchPattern = opProps.getProperty(OperationProperties.SEARCHPATTERN.name());
				    if (StringUtil.isEmpty(searchPattern))
				    	throw new ConnectorException("Search pattern is required");
		        	StringBuilder dirContents = new StringBuilder();
		        	logger.info(directoryName + " " + searchPattern);
		            for (FileIdBothDirectoryInformation f : share.list(directoryName, searchPattern)) {
		            	dirContents.append(f.getFileName()+"\r\n");
		        	}
		    		ByteArrayInputStream dataOut = new ByteArrayInputStream(dirContents.toString().getBytes());
		            response.addResult(input, OperationStatus.SUCCESS, "OK",
		            		"OK", PayloadUtil.toPayload(dataOut));
			    	break;
			    case "READ":
				    fileName = input.getDynamicProperties().get(OperationProperties.FILENAME.name());
				    if (StringUtil.isEmpty(fileName))
				    	fileName = opProps.getProperty(OperationProperties.FILENAME.name());
				    if (StringUtil.isEmpty(fileName))
				    	throw new ConnectorException("File name is required");
				    path = directoryName+fileName;
		        	StringBuilder sb = new StringBuilder();
		        	logger.info(directoryName + " " + fileName);
		            for (FileIdBothDirectoryInformation f : share.list(directoryName, fileName)) {
					    logger.info(path);
					    InputStream isOut;
						PayloadMetadata payloadMetadata = response.createMetadata();
						payloadMetadata.setTrackedProperty(OperationProperties.FILENAME.name(),  f.getFileName());
						try {
							isOut = this.readFile(share, directoryName + f.getFileName());
						} catch (IOException e) {
							throw new ConnectorException(e);
						}
						if (StringUtil.isNotBlank(OperationProperties.RENAMEAFTERREAD.name()))
						{
							File renameFile = getFile(share, directoryName + f.getFileName(), AccessMask.GENERIC_WRITE, SMB2CreateDisposition.FILE_OPEN);
							renameFile.rename(directoryName + f.getFileName() + OperationProperties.RENAMEAFTERREAD.name());
							renameFile.close();
						}
			            response.addPartialResult(input, OperationStatus.SUCCESS, "OK", "OK", PayloadUtil.toPayload(isOut, payloadMetadata));
		        	}
		            response.finishPartialResult(input);
		            
			    	break;
			    case "WRITE":
				    fileName = input.getDynamicProperties().get(OperationProperties.FILENAME.name());
				    if (StringUtil.isEmpty(fileName))
				    	fileName = UUID.randomUUID().toString()+".dat";
				    String action = opProps.getProperty(OperationProperties.IF_FILE_EXISTS.name(),OperationProperties.CREATE_UNIQUE.name());
				    boolean append=false;
				    SMB2CreateDisposition createDisposition = SMB2CreateDisposition.FILE_CREATE;
				    Set<AccessMask> accessMask =  EnumSet.of(AccessMask.FILE_WRITE_DATA);
				    long fileOffset=0;
				    switch (action)
				    {
				    case "CREATE_UNIQUE":
				    	String extension = "";
				    	String baseFileName = fileName;
				    	int extensionPos = fileName.lastIndexOf('.');
				    	if (extensionPos>-1)
				    	{
				    		baseFileName=fileName.substring(0,extensionPos);
				    		extension=fileName.substring(extensionPos);
				    	}
				    	int index=1;
				    	fileName = baseFileName+extension;
				    	while (share.fileExists(directoryName+fileName))
				    	{
				    		fileName = baseFileName + index + extension;
				    		index++;
				    	}
				    	break;
				    case "APPEND":
				    	accessMask =EnumSet.of(AccessMask.FILE_APPEND_DATA);
				    	createDisposition = SMB2CreateDisposition.FILE_OVERWRITE_IF;
				    	if (share.fileExists(directoryName+fileName))
				    	{
				    		fileOffset = share.getFileInformation(directoryName+fileName).getStandardInformation().getEndOfFile();
				    	}
				    	break;
				    case "OVERWRITE":
				    	createDisposition = SMB2CreateDisposition.FILE_OVERWRITE_IF;
				    	break;
				    case "THROWERROR":
				    	break;
				    }

//					SMB2CreateDisposition createDisposition = append ? SMB2CreateDisposition.FILE_OPEN_IF : SMB2CreateDisposition.FILE_OVERWRITE_IF;
				    path = directoryName+fileName;
				    logger.info(path);
				    File file=null;
				    is = input.getData();
				    try {
						file = this.openFileForWrite(share, path, createDisposition);
						this.inputStream2OutputStream(is, file, fileOffset);
			            response.addEmptyResult(input, OperationStatus.SUCCESS, "OK", "OK");
					} catch (IOException e) {
						throw new ConnectorException(e);
					} finally {
						IOUtils.closeQuietly(is);
						IOUtils.closeQuietly(file);						
					}
			    	break;
		    }
		    //We only allow a single input document so stop after one.
		    if ("READ".contentEquals(getContext().getCustomOperationType()))
		    	break;
	    }
	}
	
	private void inputStream2OutputStream(InputStream is, File file, long fileOffset) throws IOException
	{
	    int length;
	    byte[] buffer = new byte[1024];

	    // copy data from input stream to output stream
	    while ((length = is.read(buffer)) != -1) {
	        file.write(buffer, fileOffset, 0, length);
	    }
	    file.flush();
	    file.close();
	}
	
	private File openFileForWrite(DiskShare share, String f, SMB2CreateDisposition createDisposition) throws FileSystemException, IOException {
		Set<AccessMask> accessMask = new HashSet<AccessMask>(EnumSet.of(AccessMask.FILE_ADD_FILE));
		Set<SMB2CreateOptions> createOptions = new HashSet<SMB2CreateOptions>(
				EnumSet.of(SMB2CreateOptions.FILE_NON_DIRECTORY_FILE, SMB2CreateOptions.FILE_WRITE_THROUGH));
		
		final File file = share.openFile(f, accessMask, null, SMB2ShareAccess.ALL, createDisposition, createOptions);
		return file;
//		OutputStream out = file.getOutputStream();
//		
//		FilterOutputStream fos = new FilterOutputStream(out) {
//
//			boolean isOpen = true;
//			@Override
//			public void close() throws IOException {
//				if(isOpen) {
//					super.close();
//					isOpen=false;
//				}
//				file.close();
//			}
//		};
//		return fos;
	}
	
	private InputStream readFile(DiskShare share, String filename) throws FileSystemException, IOException {
		final File file = getFile(share, filename, AccessMask.GENERIC_READ, SMB2CreateDisposition.FILE_OPEN);
		InputStream is = file.getInputStream();
		FilterInputStream fis = new FilterInputStream(is) {

			boolean isOpen = true;
			@Override
			public void close() throws IOException {
				if(isOpen) {
					super.close();
					isOpen=false;
				}
				file.close();
			}
		};
		return fis;
	}
	
	private File getFile(DiskShare share, String filename, AccessMask accessMask, SMB2CreateDisposition createDisposition) {
		Set<SMB2ShareAccess> shareAccess = new HashSet<SMB2ShareAccess>();
		shareAccess.addAll(SMB2ShareAccess.ALL);

		Set<SMB2CreateOptions> createOptions = new HashSet<SMB2CreateOptions>();
		createOptions.add(SMB2CreateOptions.FILE_WRITE_THROUGH);
		
		Set<AccessMask> accessMaskSet = new HashSet<AccessMask>();
		accessMaskSet.add(accessMask);
		File file;

		file = share.openFile(filename, accessMaskSet, null, shareAccess, createDisposition, createOptions);
		return file;
	}
	
//	private void maybeCreate(DiskShare diskShare, String pathInShare) {
//		  String existingPrefix = FileUtil.normalizeRelativePath(pathInShare).replace('/', '\\');
//		  final Stack<String> toCreate = new Stack<>();
//
//		  while (existingPrefix.length() > 0 && !diskShare.folderExists(existingPrefix)) {
//		    final int endIndex = existingPrefix.lastIndexOf('\\');
//		    if (endIndex > -1) {
//		      toCreate.push(existingPrefix.substring(endIndex + 1));
//		      existingPrefix = existingPrefix.substring(0, endIndex);
//		    } else {
//		      toCreate.push(existingPrefix);
//		      existingPrefix = "";
//		    }
//		  }
//
//		  while (!toCreate.empty()) {
//		    existingPrefix = (existingPrefix.length() > 0 ? existingPrefix + "\\" : "") + toCreate.pop();
//		    diskShare.mkdir(existingPrefix);
//		  }
//		}
	
	@Override
    public WindowsSMBFileConnection getConnection() {
        return (WindowsSMBFileConnection) super.getConnection();
    }
}