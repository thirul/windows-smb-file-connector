package com.boomi.connector.smb;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.boomi.connector.api.OperationType;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;
import com.boomi.connector.testutil.SimpleTrackedData;

class WindowsSMBOperationTest {
	
	Map<String, Object> connProps;

	@BeforeEach
	public void initialize()
	{
		String domain = "aDomain";
		String username = "aUsername";
		String password = "aPassword";
        connProps = new HashMap<String,Object>();
		JSONObject testCredentials;
		try {
			testCredentials = new JSONObject(TestUtil.readResource("testCredentials.json", this.getClass()));
			domain = testCredentials.getString("domain");
			username = testCredentials.getString("username");
			password = testCredentials.getString("password");
		} catch (Exception e) {
		}
        connProps.put(WindowsSMBFileConnection.ConnectionProperties.HOSTNAME.toString(), "localhost");
        connProps.put(WindowsSMBFileConnection.ConnectionProperties.USERNAME.toString(), username);
        connProps.put(WindowsSMBFileConnection.ConnectionProperties.PASSWORD.toString(), password);
        connProps.put(WindowsSMBFileConnection.ConnectionProperties.DOMAIN.toString(), domain);
        connProps.put(WindowsSMBFileConnection.ConnectionProperties.SHARENAME.toString(), "c");        
	}
	
    @Test 
    public void testListOperation() throws Exception
    {
    	WindowsSMBFileConnector connector = new WindowsSMBFileConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.DIRECTORY.toString(), "Users");
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.SEARCHPATTERN.toString(), "*");

        MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, null, null, null);
        context.setCustomOperationType("LIST");
        tester.setOperationContext(context);
        List<SimpleTrackedData> trackedInputs = new ArrayList<SimpleTrackedData>();
		Map<String, String> dynamicProps = new HashMap<String, String>();
		SimpleTrackedData trackedData = new SimpleTrackedData(0, new ByteArrayInputStream("XX".getBytes()), null, dynamicProps);
        trackedInputs.add(trackedData);
        List <SimpleOperationResult> actual = tester.executeExecuteOperationWithTrackedData(trackedInputs);
        assertEquals("OK", actual.get(0).getMessage());
//        assertEquals("403",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
    }

    @Test 
    public void testReadOperation() throws Exception
    {
    	WindowsSMBFileConnector connector = new WindowsSMBFileConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.DIRECTORY.toString(), "");
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.IF_FILE_EXISTS.toString(), WindowsSMBFileExecuteOperation.OperationProperties.CREATE_UNIQUE.toString());
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.RENAMEAFTERREAD.toString(), ".DONE");
               
        MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, null, null, null);
        context.setCustomOperationType("READ");
        tester.setOperationContext(context);
        List<SimpleTrackedData> trackedInputs = new ArrayList<SimpleTrackedData>();
		Map<String, String> dynamicProps = new HashMap<String, String>();
		dynamicProps.put(WindowsSMBFileExecuteOperation.OperationProperties.FILENAME.toString(), "*.TXT");
		SimpleTrackedData trackedData = new SimpleTrackedData(0, new ByteArrayInputStream("ABCDEFG\r\n".getBytes()), null, dynamicProps);
        trackedInputs.add(trackedData);
        List <SimpleOperationResult> actual = tester.executeExecuteOperationWithTrackedData(trackedInputs);
        assertEquals(1, actual.get(0).getPayloads().size());
        assertEquals("Forbidden", actual.get(0).getMessage());
        assertEquals("403",actual.get(0).getStatusCode());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
    }
    
    @Test 
    public void testWriteUniqueOperation() throws Exception
    {
    	WindowsSMBFileConnector connector = new WindowsSMBFileConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.DIRECTORY.toString(), "");
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.IF_FILE_EXISTS.toString(), WindowsSMBFileExecuteOperation.OperationProperties.CREATE_UNIQUE.toString());
        
        MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, null, null, null);
        context.setCustomOperationType("WRITE");
        tester.setOperationContext(context);
        List<SimpleTrackedData> trackedInputs = new ArrayList<SimpleTrackedData>();
		Map<String, String> dynamicProps = new HashMap<String, String>();
		dynamicProps.put(WindowsSMBFileExecuteOperation.OperationProperties.FILENAME.toString(), "XXX.TXT");
		SimpleTrackedData trackedData = new SimpleTrackedData(0, new ByteArrayInputStream("ABCDEFG\r\n".getBytes()), null, dynamicProps);
        trackedInputs.add(trackedData);
        List <SimpleOperationResult> actual = tester.executeExecuteOperationWithTrackedData(trackedInputs);
        assertEquals("Forbidden", actual.get(0).getMessage());
        assertEquals("403",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
    }
    
    @Test 
    public void testOverwriteOperation() throws Exception
    {
    	WindowsSMBFileConnector connector = new WindowsSMBFileConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.DIRECTORY.toString(), "");
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.IF_FILE_EXISTS.toString(), WindowsSMBFileExecuteOperation.OperationProperties.OVERWRITE.toString());
        
        MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, null, null, null);
        context.setCustomOperationType("WRITE");
        tester.setOperationContext(context);
        List<SimpleTrackedData> trackedInputs = new ArrayList<SimpleTrackedData>();
		Map<String, String> dynamicProps = new HashMap<String, String>();
		dynamicProps.put(WindowsSMBFileExecuteOperation.OperationProperties.FILENAME.toString(), "XXX.TXT");
		SimpleTrackedData trackedData = new SimpleTrackedData(0, new ByteArrayInputStream("HIJK\r\n".getBytes()), null, dynamicProps);
        trackedInputs.add(trackedData);
        List <SimpleOperationResult> actual = tester.executeExecuteOperationWithTrackedData(trackedInputs);
        assertEquals("Forbidden", actual.get(0).getMessage());
        assertEquals("403",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
    }

    @Test 
    public void testWriteFailIfExistsOperation() throws Exception
    {
    	WindowsSMBFileConnector connector = new WindowsSMBFileConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.DIRECTORY.toString(), "");
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.IF_FILE_EXISTS.toString(), WindowsSMBFileExecuteOperation.OperationProperties.THROWERROR.toString());
        
        MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, null, null, null);
        context.setCustomOperationType("WRITE");
        tester.setOperationContext(context);
        List<SimpleTrackedData> trackedInputs = new ArrayList<SimpleTrackedData>();
		Map<String, String> dynamicProps = new HashMap<String, String>();
		dynamicProps.put(WindowsSMBFileExecuteOperation.OperationProperties.FILENAME.toString(), "XXX.TXT");
		SimpleTrackedData trackedData = new SimpleTrackedData(0, new ByteArrayInputStream("HIJK\r\n".getBytes()), null, dynamicProps);
        trackedInputs.add(trackedData);
        List <SimpleOperationResult> actual = tester.executeExecuteOperationWithTrackedData(trackedInputs);
        assertEquals("Forbidden", actual.get(0).getMessage());
        assertEquals("403",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
    }

    @Test 
    public void testWriteAppend() throws Exception
    {
    	WindowsSMBFileConnector connector = new WindowsSMBFileConnector();
        ConnectorTester tester = new ConnectorTester(connector);

        Map<String, Object> opProps = new HashMap<String,Object>();
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.DIRECTORY.toString(), "");
        opProps.put(WindowsSMBFileExecuteOperation.OperationProperties.IF_FILE_EXISTS.toString(), WindowsSMBFileExecuteOperation.OperationProperties.APPEND.toString());
        
        MockOperationContext context = new MockOperationContext(null, connector, OperationType.EXECUTE, connProps, opProps, null, null, null);
        context.setCustomOperationType("WRITE");
        tester.setOperationContext(context);
        List<SimpleTrackedData> trackedInputs = new ArrayList<SimpleTrackedData>();
		Map<String, String> dynamicProps = new HashMap<String, String>();
		dynamicProps.put(WindowsSMBFileExecuteOperation.OperationProperties.FILENAME.toString(), "XXX.TXT");
		SimpleTrackedData trackedData = new SimpleTrackedData(0, new ByteArrayInputStream("QRS\r\n".getBytes()), null, dynamicProps);
        trackedInputs.add(trackedData);
        List <SimpleOperationResult> actual = tester.executeExecuteOperationWithTrackedData(trackedInputs);
        assertEquals("Forbidden", actual.get(0).getMessage());
        assertEquals("403",actual.get(0).getStatusCode());
        assertEquals(1, actual.get(0).getPayloads().size());
        String responseString = new String(actual.get(0).getPayloads().get(0));
        System.out.println(responseString);
    }
}
